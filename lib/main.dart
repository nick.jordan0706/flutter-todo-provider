import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import './screens/home_screen.dart';
import './providers/task.dart';
import './screens/new_todo.dart';
import './utils/constants.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => TaskProvider(),
      child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Flutter TODO',
          theme: ThemeData(
            primaryColor: Colors.yellow[800],
            accentColor: Colors.orangeAccent[700],
            fontFamily: 'Lato',
            textTheme: ThemeData.light().textTheme.copyWith(
                headline2: TextStyle(
                  color: Colors.purple,
                  fontFamily: 'Lato',
                  fontWeight: FontWeight.bold,
                  fontSize: 28,
                ),
                subtitle1: TextStyle(
                  color: Colors.purple,
                  fontFamily: 'Lato',
                  fontWeight: FontWeight.bold,
                  fontSize: 18,
                ),
              ),
            visualDensity: VisualDensity.adaptivePlatformDensity,
            appBarTheme: AppBarTheme(
              textTheme: ThemeData.light().textTheme.copyWith(
                    headline3: TextStyle(
                      color: Colors.black,
                      fontFamily: 'Lato',
                      fontSize: 22,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
            ),
          ),
          initialRoute: Constants.HOME_SCREEN,
          routes: <String, WidgetBuilder>{
            Constants.HOME_SCREEN: (BuildContext context) => HomeScreen(),
            Constants.NEW_TODO: (BuildContext context) => CreateTodo()
          }),
    );
  }
}
