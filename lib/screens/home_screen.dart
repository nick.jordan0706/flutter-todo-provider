import 'package:FlutterTODO/screens/new_todo.dart';
import 'package:flutter/material.dart';

import '../providers/task.dart';
import '../widgets/list.dart';

class HomeScreen extends StatefulWidget {
  final Task task;
  HomeScreen({this.task});
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'TO-DO LIST',
          style: TextStyle(
            fontFamily: 'Lato',
            fontWeight: FontWeight.bold,
            fontSize: 18,
          ),
        ),
      ),
      body: List(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => CreateTodo(
                  isEditMode: false,
                ),
              ));
        },
        tooltip: 'Add a new task!',
      ),
    );
  }
}
