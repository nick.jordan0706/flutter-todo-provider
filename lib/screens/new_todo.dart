import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

import '../providers/task.dart';

class CreateTodo extends StatefulWidget {
  final String id;
  final bool isEditMode;

  CreateTodo({Key key, this.id, this.isEditMode}) : super(key: key);

  @override
  _CreateTodoState createState() => _CreateTodoState();
}

class _CreateTodoState extends State<CreateTodo> {
  Task task;
  TimeOfDay _selectedTime;
  DateTime _selectedStartDate;
  DateTime _selectedEndDate;
  String _inputDescription;
  final _formKey = GlobalKey<FormState>();

  void _pickStartDate() {
    showDatePicker(
      context: context,
      initialDate: widget.isEditMode ? _selectedStartDate : DateTime.now(),
      firstDate: DateTime(2020),
      lastDate: DateTime(2030),
    ).then((date) {
      if (date == null) {
        return;
      }
      date = date;
      setState(() {
        _selectedStartDate = date;
      });
    });
  }

  void _pickEndDate() {
    showDatePicker(
      context: context,
      initialDate: widget.isEditMode ? _selectedEndDate : DateTime.now(),
      firstDate: DateTime(2020),
      lastDate: DateTime(2030),
    ).then((date) {
      if (date == null) {
        return;
      }
      date = date;
      setState(() {
        _selectedEndDate = date;
      });
    });
  }

  void _pickDueTime() {
    showTimePicker(
      context: context,
      initialTime: widget.isEditMode ? _selectedTime : TimeOfDay.now(),
    ).then((time) {
      if (time == null) {
        return;
      }
      setState(() {
        _selectedTime = time;
      });
    });
  }

  void _validateForm() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      if (_selectedStartDate == null && _selectedEndDate == null) {
        _selectedStartDate = DateTime.now();
        _selectedEndDate = DateTime.now();
      }
      try {
        if (widget.isEditMode == false) {
          Provider.of<TaskProvider>(context, listen: false).createNewTask(
            Task(
                id: DateTime.now().toString(),
                description: _inputDescription,
                startDate: _selectedStartDate,
                endDate: _selectedEndDate,
                dueTime: _selectedTime),
          );
        } else {
          Provider.of<TaskProvider>(context, listen: false).editTask(
            Task(
              id: task.id,
              description: _inputDescription,
              startDate: _selectedStartDate,
              endDate: _selectedEndDate,
              dueTime: _selectedTime,
            ),
          );
        }
        Navigator.of(context).pop();
      } catch (e) {
        print(e);
      }
    }
  }

  @override
  void initState() {
    if (widget.isEditMode) {
      task =
          Provider.of<TaskProvider>(context, listen: false).getById(widget.id);
      _selectedStartDate = task.startDate;
      _selectedEndDate = task.endDate;
      _selectedTime = task.dueTime;
      _inputDescription = task.description;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'ADD NEW TO-DO LIST',
          style: TextStyle(
            fontFamily: 'Lato',
            fontWeight: FontWeight.bold,
            fontSize: 18,
          ),
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Title',
                style: Theme.of(context).textTheme.subtitle1,
              ),
              TextFormField(
                initialValue:
                    _inputDescription == null ? null : _inputDescription,
                decoration: InputDecoration(
                  hintText: 'Describe Your task',
                ),
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter description';
                  }
                  return null;
                },
                onSaved: (value) {
                  _inputDescription = value;
                },
              ),
              SizedBox(
                height: 20,
              ),
              Text('Start Date', style: Theme.of(context).textTheme.subtitle1),
              TextFormField(
                onTap: () {
                  _pickStartDate();
                },
                readOnly: true,
                decoration: InputDecoration(
                  hintText: _selectedStartDate == null
                      ? 'Provide your start date'
                      : DateFormat.yMMMd()
                          .format(_selectedStartDate)
                          .toString(),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Text('End Date', style: Theme.of(context).textTheme.subtitle1),
              TextFormField(
                onTap: () {
                  _pickEndDate();
                },
                readOnly: true,
                decoration: InputDecoration(
                  hintText: _selectedEndDate == null
                      ? 'Provide your end date'
                      : DateFormat.yMMMd().format(_selectedEndDate).toString(),
                ),
              ),
              Container(
                alignment: Alignment.bottomRight,
                child: FlatButton(
                  child: Text(
                    !widget.isEditMode ? 'ADD TASK' : 'EDIT TASK',
                    style: TextStyle(
                        color: Theme.of(context).accentColor,
                        fontFamily: 'Lato',
                        fontSize: 18,
                        fontWeight: FontWeight.bold),
                  ),
                  onPressed: () {
                    _validateForm();
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
