# FlutterTODO with Provider

**This application starts with a simple homepage for displaying the list of tasks created. To create a new task, user will be redirected to a new blank form to input To-do title, start date, end date. Tasks can be edited and checked off once it's done. Tasks can be deleted by swiping from left to right.**
